package com.cbs.moneypicktest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cbs.moneypicktest.api_response.GenericError;
import com.cbs.moneypicktest.api_response.LoginSuccess;
import com.cbs.moneypicktest.utils.AppPreferences;
import com.cbs.moneypicktest.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Calendar;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public class LoginActivity extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword;

    AppPreferences appPreferences;
    MaterialDialog MaterialProgressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        appPreferences = new AppPreferences(getApplicationContext());
        MaterialProgressbar = new MaterialDialog.Builder(this)
                .content(getString(R.string.loading_text))
                .cancelable(false)
                .progress(true, 0)
                .build();

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        Button btnSignIn = (Button) findViewById(R.id.btnSignIn);

        etEmail.setText(Constants.USER_TEST);
        etPassword.setText(Constants.USER_PASS);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String timestamp = String.valueOf(getTimestamp());
                String sSign = getSsign(timestamp, Constants.API_KEY);

                if (email.equals("")) {
                    Toast.makeText(getApplicationContext(), "Email can't no be empty.", Toast.LENGTH_LONG).show();
                } else if (password.equals("")) {
                    Toast.makeText(getApplicationContext(), "Password can't no be empty.", Toast.LENGTH_LONG).show();
                } else {
                    RequestParams params = new RequestParams();
                    params.put("email", email);
                    params.put("password", password);
                    params.put("timestamp", timestamp);
                    params.put("ssign", sSign);

                    Log.d(Constants.TAG, "email: " + email);
                    Log.d(Constants.TAG, "password: " + password);
                    Log.d(Constants.TAG, "timestamp: " + timestamp);
                    Log.d(Constants.TAG, "sSign: " + sSign);

                    make_login(params);
                }
            }
        });
    }

    private void make_login(RequestParams params) {

        MaterialProgressbar.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(3, 3000);
        client.setTimeout(20000);
        client.post(Constants.URL_LOGIN, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d(Constants.TAG, "LoginSuccess: " + response);
                    Gson gson = new GsonBuilder().create();
                    LoginSuccess login_response = gson.fromJson(response.toString(), LoginSuccess.class);
                    appPreferences.setString(Constants.TOKEN_KEY, login_response.getData().getToken());
                    MaterialProgressbar.dismiss();

                    String msg = login_response.getMsg();
                    if (msg != null && !msg.equals("")) {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }

                    goToPaymentActivity();
                } catch (Exception e) {
                    MaterialProgressbar.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                MaterialProgressbar.dismiss();
                try {
                    Log.d(Constants.TAG, "LoginError: " + errorResponse);
                    Gson gson = new GsonBuilder().create();
                    GenericError genericError = gson.fromJson(errorResponse.toString(), GenericError.class);
                    String msg = genericError.getMsg();
                    if (msg != null && !msg.equals("")) {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                MaterialProgressbar.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                MaterialProgressbar.dismiss();
            }
        });
    }

    private void goToPaymentActivity() {
        Intent i = new Intent(this, PaymentCreateActivity.class);
        startActivity(i);
    }

    public static String getSsign(String timeStamp, String ApiKey) {
        try{
            return SHA256(timeStamp + ApiKey);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public static Integer getTimestamp() {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        String aux = "" + currentTimestamp.getTime();
        aux = aux.substring(0, 10);
        return Integer.parseInt(aux);
    }

    public static String SHA256 (String text) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(text.getBytes());
        byte[] digest = md.digest();

        StringBuilder sb = new StringBuilder();
        for (byte aDigest : digest) {
            String hex = Integer.toHexString(0xFF & aDigest);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
}
