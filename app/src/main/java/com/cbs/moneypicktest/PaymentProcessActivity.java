package com.cbs.moneypicktest;


import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cbs.moneypicktest.utils.Constants;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public class PaymentProcessActivity extends AppCompatActivity {

    private MaterialDialog MaterialProgressbar;
    private String hashed;
    private String payment_url;
    private WebView webViewContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_process);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            hashed = extras.getString("hashed", "");
            payment_url = Constants.WEB_URL_PAYMENT + hashed + "/" + Constants.TOKEN_USER_TEST;
        } else {
            finish();
        }

        MaterialProgressbar = new MaterialDialog.Builder(this)
                .content(getString(R.string.loading_text))
                .cancelable(false)
                .progress(true, 0).build();
        MaterialProgressbar.show();

        webViewContent = (WebView) findViewById(R.id.webViewContent);

        WebSettings webSettings = webViewContent.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        webViewContent.setWebViewClient(new MyWebViewClient());
        webViewContent.loadUrl(payment_url);

        Log.d(Constants.TAG, "payment_url: " + payment_url);

    }

    @Override
    public void onBackPressed() {
        // Handle when the user exits without complete payment
        Toast.makeText(getApplicationContext(), "The user left the payment", Toast.LENGTH_LONG).show();
        finish();
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(Constants.TAG, "url finished: " + url);
            MaterialProgressbar.dismiss();

            if(url.equals(Constants.URL_WEB_REDIRECT)){
                // Handle the response of the successful payment
                webViewContent.loadData("", "text/html", null);
                Toast.makeText(getApplicationContext(), "Payment Finished", Toast.LENGTH_LONG).show();
                finish();
            }else if(url.equals(Constants.URL_ERROR)){
                // Handle the response of the cancelled payment
                webViewContent.loadData("", "text/html", null);
                Toast.makeText(getApplicationContext(), "An error has occurred.", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            webViewContent.loadData("", "text/html", null);
            Log.d(Constants.TAG, "onReceivedError");
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            webViewContent.loadData("", "text/html", null);
            Log.d(Constants.TAG, "onReceivedError");
        }
    }
}
