package com.cbs.moneypicktest.api_response;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public class GenericError {
    private String msg;
    private Object data;
    private String code;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public Object getData() {
        return data;
    }

    public String getCode() {
        return code;
    }
}
