package com.cbs.moneypicktest.api_response;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public class LoginSuccess {

    private String msg;
    private DataEntity data;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private String modification;
        private String provider_user_id;
        private String token;
        private UserEntity user;
        private String provider_token;
        private String provider_lastcheck;
        private boolean active;
        private DataValue data;
        private String creation;
        private String refresh_token;
        private String provider;
        private boolean have_pin;
        private String expiration;

        public void setModification(String modification) {
            this.modification = modification;
        }

        public void setProvider_user_id(String provider_user_id) {
            this.provider_user_id = provider_user_id;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public void setUser(UserEntity user) {
            this.user = user;
        }

        public void setProvider_token(String provider_token) {
            this.provider_token = provider_token;
        }

        public void setProvider_lastcheck(String provider_lastcheck) {
            this.provider_lastcheck = provider_lastcheck;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public void setData(DataValue data) {
            this.data = data;
        }

        public void setCreation(String creation) {
            this.creation = creation;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public void setHave_pin(boolean have_pin) {
            this.have_pin = have_pin;
        }

        public void setExpiration(String expiration) {
            this.expiration = expiration;
        }

        public String getModification() {
            return modification;
        }

        public String getProvider_user_id() {
            return provider_user_id;
        }

        public String getToken() {
            return token;
        }

        public UserEntity getUser() {
            return user;
        }

        public String getProvider_token() {
            return provider_token;
        }

        public String getProvider_lastcheck() {
            return provider_lastcheck;
        }

        public boolean isActive() {
            return active;
        }

        public DataValue getData() {
            return data;
        }

        public String getCreation() {
            return creation;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public String getProvider() {
            return provider;
        }

        public boolean isHave_pin() {
            return have_pin;
        }

        public String getExpiration() {
            return expiration;
        }

        public static class DataValue {
            private String ip;

            public void setIp(String ip) {
                this.ip = ip;
            }

            public String getIp() {
                return ip;
            }
        }
    }


    public static class UserEntity {
        private String country;
        private String language;
        private String phone;
        private String email;
        private int id;
        private String last_name;
        private boolean is_commerce;
        private String commerce_name;
        private String first_name;
        private String profile_photo;

        public void setCountry(String country) {
            this.country = country;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public void setIs_commerce(boolean is_commerce) {
            this.is_commerce = is_commerce;
        }

        public void setCommerce_name(String commerce_name) {
            this.commerce_name = commerce_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public void setProfile_photo(String profile_photo) {
            this.profile_photo = profile_photo;
        }

        public String getCountry() {
            return country;
        }

        public String getLanguage() {
            return language;
        }

        public String getPhone() {
            return phone;
        }

        public String getEmail() {
            return email;
        }

        public int getId() {
            return id;
        }

        public String getLast_name() {
            return last_name;
        }

        public boolean isIs_commerce() {
            return is_commerce;
        }

        public String getCommerce_name() {
            return commerce_name;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getProfile_photo() {
            return profile_photo;
        }
    }
}
