package com.cbs.moneypicktest.api_response;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public class PaymentSuccess {

    private String msg;
    private DataEntity data;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private int id;
        private String creation;
        private String currency;
        private TransferEntity transfer;
        private PayerEntity payer;
        private String approved_url;
        private String description;
        private String status;
        private int commerce;
        private String modification;
        private String webhook_url;
        private String cancelled_url;
        private String amount;
        private int webhook_retries;
        private String webhook_checkafter;
        private String hashed;
        private String expiration;
        private String supplied_id;

        public void setId(int id) {
            this.id = id;
        }

        public void setCreation(String creation) {
            this.creation = creation;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setTransfer(TransferEntity transfer) {
            this.transfer = transfer;
        }

        public void setPayer(PayerEntity payer) {
            this.payer = payer;
        }

        public void setApproved_url(String approved_url) {
            this.approved_url = approved_url;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public void setCommerce(int commerce) {
            this.commerce = commerce;
        }

        public void setModification(String modification) {
            this.modification = modification;
        }

        public void setWebhook_url(String webhook_url) {
            this.webhook_url = webhook_url;
        }

        public void setCancelled_url(String cancelled_url) {
            this.cancelled_url = cancelled_url;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public void setWebhook_retries(int webhook_retries) {
            this.webhook_retries = webhook_retries;
        }

        public void setWebhook_checkafter(String webhook_checkafter) {
            this.webhook_checkafter = webhook_checkafter;
        }

        public void setHashed(String hashed) {
            this.hashed = hashed;
        }

        public void setExpiration(String expiration) {
            this.expiration = expiration;
        }

        public void setSupplied_id(String supplied_id) {
            this.supplied_id = supplied_id;
        }

        public int getId() {
            return id;
        }

        public String getCreation() {
            return creation;
        }

        public String getCurrency() {
            return currency;
        }

        public TransferEntity getTransfer() {
            return transfer;
        }

        public PayerEntity getPayer() {
            return payer;
        }

        public String getApproved_url() {
            return approved_url;
        }

        public String getDescription() {
            return description;
        }

        public String getStatus() {
            return status;
        }

        public int getCommerce() {
            return commerce;
        }

        public String getModification() {
            return modification;
        }

        public String getWebhook_url() {
            return webhook_url;
        }

        public String getCancelled_url() {
            return cancelled_url;
        }

        public String getAmount() {
            return amount;
        }

        public int getWebhook_retries() {
            return webhook_retries;
        }

        public String getWebhook_checkafter() {
            return webhook_checkafter;
        }

        public String getHashed() {
            return hashed;
        }

        public String getExpiration() {
            return expiration;
        }

        public String getSupplied_id() {
            return supplied_id;
        }

        public static class TransferEntity {
            private String id;

            public void setId(String id) {
                this.id = id;
            }

            public String getId() {
                return id;
            }
        }

        public static class PayerEntity {
            private String id;

            public void setId(String id) {
                this.id = id;
            }

            public String getId() {
                return id;
            }
        }
    }
}
