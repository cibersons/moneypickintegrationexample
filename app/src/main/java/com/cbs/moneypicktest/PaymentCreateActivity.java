package com.cbs.moneypicktest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cbs.moneypicktest.api_response.GenericError;
import com.cbs.moneypicktest.api_response.PaymentSuccess;
import com.cbs.moneypicktest.utils.AppPreferences;
import com.cbs.moneypicktest.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public class PaymentCreateActivity extends AppCompatActivity {

    private EditText etAmount;
    private EditText etDescription;

    AppPreferences appPreferences;
    MaterialDialog MaterialProgressbar;

    PaymentSuccess payment;
    private boolean is_app_installed = appInstalledOrNot();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_create);


        appPreferences = new AppPreferences(getApplicationContext());
        MaterialProgressbar = new MaterialDialog.Builder(this)
                .content(getString(R.string.loading_text))
                .cancelable(false)
                .progress(true, 0)
                .build();


        etAmount = (EditText) findViewById(R.id.etAmount);
        etDescription = (EditText) findViewById(R.id.etDescription);
        Button btnPayment = (Button) findViewById(R.id.btnPayment);

        etAmount.setText(Constants.AMOUNT_TEST);
        etDescription.setText(Constants.DESCRIPTION_TEST);

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String amount = etAmount.getText().toString();
                String description = etDescription.getText().toString();

                if (amount.equals("")) {
                    Toast.makeText(getApplicationContext(), "Amount can't no be empty.", Toast.LENGTH_LONG).show();
                } else if (description.equals("")) {
                    Toast.makeText(getApplicationContext(), "Description can't no be empty.", Toast.LENGTH_LONG).show();
                } else {
                    RequestParams params = new RequestParams();
                    params.put("currency", "PYG"); // Use for test purpose
                    params.put("amount", amount);
                    params.put("description", description);
                    if (is_app_installed) {
                        params.put("redirect_url", Constants.URL_APP_REDIRECT);
                    } else {
                        params.put("redirect_url", Constants.URL_WEB_REDIRECT);
                    }
                    params.put("webhook_url", Constants.URL_WEBHOOK);

                    make_payment(params);
                }
            }
        });
    }

    private void make_payment(RequestParams params) {

        MaterialProgressbar.show();

        Log.d(Constants.TAG, "Authorization bearer " + appPreferences.getString(Constants.TOKEN_KEY, ""));

        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(3, 3000);
        client.setTimeout(60000);
        client.addHeader("Authorization", "bearer " + appPreferences.getString(Constants.TOKEN_KEY, ""));
        client.post(Constants.URL_COMMERCE_PAYMENT, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d(Constants.TAG, "PaymentCreateSuccess: " + response);
                    Gson gson = new GsonBuilder().create();
                    payment = gson.fromJson(response.toString(), PaymentSuccess.class);
                    MaterialProgressbar.dismiss();

                    String msg = payment.getMsg();
                    if (msg != null && !msg.equals("")) {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }

                    goToPaymentProcessActivity();
                } catch (Exception e) {
                    MaterialProgressbar.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                MaterialProgressbar.dismiss();
                try {
                    Log.d(Constants.TAG, "GenericError: " + errorResponse);
                    Gson gson = new GsonBuilder().create();
                    GenericError genericError = gson.fromJson(errorResponse.toString(), GenericError.class);
                    String msg = genericError.getMsg();
                    if (msg != null && !msg.equals("")) {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                MaterialProgressbar.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                MaterialProgressbar.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 100){
            Toast.makeText(getApplicationContext(), "Payment Finished", Toast.LENGTH_LONG).show();
        }

        if(requestCode == 250){
            final Intent intent = getIntent();
            if(intent != null) {
                final String action = intent.getAction();
                Log.d(Constants.TAG, "action: " + action);
                if (Intent.ACTION_VIEW.equals(action)) {
                    final List<String> segments = intent.getData().getPathSegments();

                    if (intent.getData().getEncodedPath() == null) {
                        Log.d(Constants.TAG, "Path null!!");
                    } else if (segments == null) {
                        Log.d(Constants.TAG, "Segments null!!");
                    } else if (intent.getData().getEncodedPath().contains("/payment/") && segments.size() > 1) {
                        Log.d(Constants.TAG, "Path: " + intent.getData().getEncodedPath());
                        Log.d(Constants.TAG, "Segments: " + segments.get(1));
                    }
                }
            }else{
                Log.d(Constants.TAG, "Intent Data Null!!!");
            }
            Toast.makeText(getApplicationContext(), "Payment Finished from Browser", Toast.LENGTH_LONG).show();
        }
    }

    private void goToPaymentProcessActivity() {

        if(is_app_installed){
            try{
                String params = "token_transfer="+ payment.getData().getHashed() +"&" +
                        "token_oauth="+ Constants.TOKEN_USER_TEST +"&";

                String url = "android://money-pick.com/commerce-payment?" + params;

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivityForResult(browserIntent, 100);
            }catch (Exception e){e.printStackTrace();}
        }else{
            /*Intent i = new Intent(this, PaymentProcessActivity.class);
            i.putExtra("hashed", payment.getData().getHashed());
            startActivity(i);*/
            //String url = Constants.WEB_URL_PAYMENT + payment.getData().getHashed() + "/" + Constants.TOKEN_USER_TEST;
            String url = "http://192.168.1.205:7000/desarrollo/mp_client_test/index.php";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivityForResult(i, 250);
        }
    }

    private boolean appInstalledOrNot() {
        try{
            String uri = "com.cibersons.money_pick";
            PackageManager pm = getPackageManager();
            boolean app_installed;
            try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
                app_installed = true;
            }
            catch (PackageManager.NameNotFoundException e) {
                app_installed = false;
            }
            return app_installed;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
