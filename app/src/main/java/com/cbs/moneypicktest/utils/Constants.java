package com.cbs.moneypicktest.utils;

/**
 * Created by Victor Centurion on 31/1/16.
 *
 */
public final class Constants {

    public static final String USER_TEST = "cbstests01@gmail.com";
    public static final String USER_PASS = "123456";

    public static final String AMOUNT_TEST= "5000";
    public static final String DESCRIPTION_TEST= "SERVICIO DE PLOMERIA";

    public static final String TAG = "MoneyPick";
    public static final String TOKEN_KEY = "TOKEN";


    public static final String TOKEN_USER_TEST = "48dcf080de5903ca5164d654e40c734a74dacb15ca3b262682e11778794c4f0767664400a5f9f96422873aee8cb1d7d148b5c29aea0698ac7be1f151b5c4da68";

    public static final String URL_APP_REDIRECT = "http://monewpick-lib-app-url.com/redirect_url"; //http://monewpick-lib-app-url/redirect_url
    public static final String URL_WEB_REDIRECT = "http://monewpick-lib-web-url.com/redirect_url"; //http://monewpick-lib-web-url/redirect_url
    public static final String URL_ERROR = "http://localhost/error"; //http://localhost/cancelled
    public static final String URL_WEBHOOK = "http://localhost/webhook";

    public static final String API_KEY = "ae0f1587-90ff-4e45-9cdb-b5128132ca39";

    public static final String URL_BASE= "http://192.168.1.205:9100"; // http://development.money-pick.com
    public static final String URL_LOGIN = URL_BASE + "/auth/login";
    public static final String URL_COMMERCE_PAYMENT = URL_BASE + "/commerces/charges";

    public static final String WEB_URL_BASE = "http://192.168.1.205:8100/#"; // http://development.money-pick.com/app#
    public static final String WEB_URL_PAYMENT = WEB_URL_BASE + "/commerce-payment/";
}
